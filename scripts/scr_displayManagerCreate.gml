///scr_displayManagerCreate

scr_init();

ideal_width = 0;
ideal_height = 180;
global.zoom = 1;
maxZoom = 8;
display_width=display_get_width();
display_height=display_get_height();
global.autoSetAspectRatio = false;
settingsChanged = false;
ar_counter = 1;

dar[3] = 2.33;
dar[2] = 1.6;
dar[1] = 1.78;
dar[0] = 1.33;

if (global.autoSetAspectRatio)
{
    aspect_ratio = display_get_width()/display_get_height();

    switch(aspect_ratio)
    {
        case (4/3):
            ar_counter = 0;            
        break;
        case (16/9):
        default:
            ar_counter = 1;
            ideal_height = 180;
        break;
        case (16/10):
            ar_counter = 2;
            ideal_height = 200;
        break;
        case (21/9):
            ar_counter = 3;
            ideal_height = 180;
        break;
    }
}
else
{

    //default to 16x9  
    ar_counter = 1;  
    aspect_ratio = dar[ar_counter];  
}
scr_setIdealDisplay(dar[ar_counter]);
global.aspectRatioString = global.ratio[ar_counter];    
    
room_goto_next();
