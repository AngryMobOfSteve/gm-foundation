///scr_checkGamepad

var gp_num = gamepad_get_device_count();
for (var i = 0; i < gp_num; i++;)
{
    if gamepad_is_connected(i) 
    {    
        global.useGamepad = true;
        global.gpSlot = i; 
        show_debug_message("gamepad connected to slot: " + string(global.gpSlot));
    }
    //else global.useGamepad = false;
}
