
var options = 0;
switch(argument[0])
{
    case menu.disabled:
    break;
    case menu.title:
        options[0] = "Press Start";
    break;
    case menu.start:
        options[3] = "QUIT";
        options[2] = "SETTINGS";
        options[1] = "PLAY";
        options[0] = "";
    break;
    case menu.settings:
        options[4] = "CONTROLS";
        options[3] = "AUDIO";
        options[2] = "VISUAL";
        options[1] = "GAMEPLAY";
        options[0] = "SETTINGS";
    break;        
    case menu.gameplay:
        options[1] = "DIFFICULTY";
        options[0] = "GAMEPLAY";
    break;
    case menu.visual:
//        options[4] = "BORDERLESS";
        options[3] = "FULLSCREEN";
        options[2] = "RESOLUTION"; 
        options[1] = "ASPECT RATIO";        
        options[0] = "VISUAL";
    break;
    case menu.audio:
        options[3] = "SFX";
        options[2] = "MUSIC";
        options[1] = "Master Volume";
        options[0] = "AUDIO";        
    break;
    case menu.controls:
        options[2] = "GAMEPAD"
        options[1] = "REBIND";
        options[0] = "CONTROLS";
    break;
    case menu.pause:
        options[2] = "QUIT";
        options[1] = "SETTINGS";
        options[0] = "PAUSE";
    break;
    case menu.quit:
        options[2] = "YES";
        options[1] = "NO";
        options[0] = "QUIT?";
    break;

}

return options;
