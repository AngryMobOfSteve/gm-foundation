///scrGetAxisValue(stick, axis)

var stick = argument0;
var axis = argument1;

switch(stick)
{
    case "right":
        vert_axis = gp_axisrv;
        hor_axis = gp_axisrh;
    break;
    case "left":
    default:
        vert_axis = gp_axislv;
        hor_axis = gp_axislh;
    break;
}

var xstick = 0;
var ystick = 0;
var stickdir = -1;
var dist = 0;


    
if global.useGamepad 
{

    xstick = gamepad_axis_value(global.gpSlot, hor_axis);
    ystick = gamepad_axis_value(global.gpSlot, vert_axis);   
    
}

if (axis == "x")
{
    return xstick;
}
else if (axis == "y" )
{
    return ystick;
}




