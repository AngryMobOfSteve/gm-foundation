///scr_displayChangeZoom(_zoomValue)
var _zoomKey = argument0;
with (objDisplayManager)
{
    global.zoom += _zoomKey;
    global.zoom = clamp(global.zoom, 1, maxZoom);
    settingsChanged = true;
}
