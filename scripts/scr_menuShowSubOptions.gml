///scr_menuShowSubOptions(menuState, subState)

str = "";
switch(argument0){
    case menu.visual:
        switch(argument1) {
            case visual.aspect_ratio:           
                str = "     " + global.aspectRatioString;
            break;
            case visual.resolution:
                str = "    " + string(surface_get_width(application_surface))+" x "+string(surface_get_height(application_surface));
            break;
            case visual.fullscreen:
                if (window_get_fullscreen())
                    str = "     On";
                else
                    str = "     Off";
            break;
        }
    break;    
}

return str;
