///scr_setIdealDisplay(aspect_ratio)
var _ar = argument0;
switch(_ar)
{    
    case (1.33):
        ideal_height = 180;
    break;
    case (1.78):
    default:
        ideal_height = 180;
    break;
    case (1.6):
        ideal_height = 200;
    break;
    case (2.33):
    case (2.37):
    case (2.38):
        if (global.zoom <= 2)
        {
           _ar = 2.33;
           ideal_height = 180;
        }
        else {
        _ar = 2.37;
        ideal_height = 180; }
    break;
}

ideal_width = round(ideal_height* _ar);
//ideal_height = round(ideal_width / aspect_ratio);

if (global.autoSetAspectRatio) {
    if (display_width mod ideal_width != 0)
    {
        var d = round(display_width/ideal_width);
        ideal_width = display_width/d;
    }

    if (display_height mod ideal_height != 0)
    {
        var d = round(display_height/ideal_height);
        ideal_height = display_height/d;
    }
}

//check for odd numbers
if (ideal_width & 1)
    ideal_width++;

if(ideal_height & 1)
    ideal_height++;
    
//maxZoom = floor(display_get_width()/ideal_width);     
    
show_debug_message("IW: " + string(ideal_width) + ", IH: " + string(ideal_height));

for(var i = 1; i <= room_last; i++) {
   if(room_exists(i)){
       room_set_view(i, 0, true, 0, 0, ideal_width, ideal_height, 0, 0, ideal_width, ideal_height,
       0, 0, 0, 0, -1);
       room_set_view_enabled(i, true);
   }
}

var _zoomWidth = ideal_width * global.zoom;
if (_ar > 2 ) && (global.zoom > 2)
{
    if ((ideal_height * global.zoom) mod 720 == 0)
    {
        ideal_width = 1720;
        _zoomWidth = ideal_width * (( ideal_height * global.zoom ) div 720);
    }
    else
    _zoomWidth = floor(_zoomWidth / 10) * 10;
}

window_set_size(_zoomWidth,ideal_height*global.zoom);
display_set_gui_size(_zoomWidth,ideal_height*global.zoom);
surface_resize(application_surface,_zoomWidth,ideal_height*global.zoom);
alarm[0] = 1;  
