///scr_displayManagerStep

//var _zoomKey = keyboard_check_pressed(vk_up) - keyboard_check_pressed(vk_down);
var _zoomKey = 0;

var _arKey = keyboard_check_pressed(vk_right) - keyboard_check_pressed(vk_left);

if (keyboard_check_pressed(vk_f12))
{
    global.autoSetAspectRatio = !global.autoSetAspectRatio;
}


if (_zoomKey != 0)
{
    global.zoom += _zoomKey;
    global.zoom = clamp(global.zoom, 1, maxZoom);
    settingsChanged = true;
}

if (settingsChanged)
{
    scr_setIdealDisplay(dar[ar_counter]);
    settingsChanged = false;
    global.aspectRatioString = global.ratio[ar_counter];    
}


